<?php
require_once "../vendor/autoload.php";
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Platforms\MySqlPlatform;

namespace testCiCd;

class Connexion
{

    public function __construct($dbName="",$dbLogin="",$dbPw="",$dbHost="")
    {

        try
        {

            if (file_exists(__DIR__.'/.htLogin.ini')){
                $dbconf = parse_ini_file(__DIR__.'/.htLogin.ini');
            }else {

                $dbconf['dbName']   = $dbName;
                $dbconf['dbLogin']  = $dbLogin;
                $dbconf['dbPw']     = $dbPw;
                $dbconf['dbHost']   = $dbHost;
                
            }
            $config = new \Doctrine\DBAL\Configuration();

            $connectionParams = array(
                'dbname'    => $dbconf['dbName'],
                'user'      => $dbconf['dbLogin'],
                'password'  => $dbconf['dbPw'],
                'host'      => $dbconf['dbHost'],
                'driver'    => 'pdo_mysql',
                'charset'   => 'utf8',
                'driverOptions' => array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                )
            );
            $this->mysql = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        }
        catch(PDOException $e)
        {

            throw new ConnexionException($e -> getMessage());

        }

        $databasePlatform   = $this->mysql->getDatabasePlatform();
        $schemaManager      = $this->mysql->getSchemaManager();
        $queryBuilder       = $this->mysql->createQueryBuilder();
    }



}